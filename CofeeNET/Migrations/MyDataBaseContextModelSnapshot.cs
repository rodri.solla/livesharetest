﻿// <auto-generated />
using System;
using CofeeNET.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CofeeNET.Migrations
{
    [DbContext(typeof(MyDataBaseContext))]
    partial class MyDataBaseContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.3-servicing-35854")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CofeeNET.Models.CoffeeCapsule", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CapsulesLeft");

                    b.Property<int?>("CoffeeRoomID");

                    b.Property<int>("Flavour");

                    b.HasKey("ID");

                    b.HasIndex("CoffeeRoomID");

                    b.ToTable("CoffeeCapsules");
                });

            modelBuilder.Entity("CofeeNET.Models.CoffeeRoom", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Building");

                    b.Property<int>("Floor");

                    b.HasKey("ID");

                    b.ToTable("CoffeeRooms");
                });

            modelBuilder.Entity("CofeeNET.Models.CoffeeCapsule", b =>
                {
                    b.HasOne("CofeeNET.Models.CoffeeRoom")
                        .WithMany("CoffeeRoomCapsules")
                        .HasForeignKey("CoffeeRoomID");
                });
#pragma warning restore 612, 618
        }
    }
}
