﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CofeeNET.Migrations
{
    public partial class AddedEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Flavour",
                table: "CoffeeCapsules",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Flavour",
                table: "CoffeeCapsules",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
