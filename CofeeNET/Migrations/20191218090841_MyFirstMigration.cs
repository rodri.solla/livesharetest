﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CofeeNET.Migrations
{
    public partial class MyFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CoffeeRooms",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Building = table.Column<string>(nullable: true),
                    Floor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoffeeRooms", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CoffeeCapsules",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Flavour = table.Column<string>(nullable: true),
                    CapsulesLeft = table.Column<int>(nullable: false),
                    CoffeeRoomID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoffeeCapsules", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CoffeeCapsules_CoffeeRooms_CoffeeRoomID",
                        column: x => x.CoffeeRoomID,
                        principalTable: "CoffeeRooms",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoffeeCapsules_CoffeeRoomID",
                table: "CoffeeCapsules",
                column: "CoffeeRoomID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoffeeCapsules");

            migrationBuilder.DropTable(
                name: "CoffeeRooms");
        }
    }
}
