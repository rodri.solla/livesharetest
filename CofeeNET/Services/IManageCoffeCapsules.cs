using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CofeeNET.Services
{
    public interface IManageCoffeCapsules
    {
        Task<List<Models.CoffeeRoom>> GetCoffeeRooms();
        Task<List<Models.CoffeeCapsule>> GetCoffeeCapsules(int CoffeeRoomID);
        Task<bool> RefillCoffeeCapsules(string capsuleFlavour, int CoffeeRoomID);
        Task<bool> ConsumeCoffeeCapsule(Models.CoffeFlavors capsuleFlavour, int CoffeeRoomID);
        Task<Models.CoffeeRoom> AddCoffeeRoom(Models.CoffeeRoom room);
        Task<Models.CoffeeCapsule> AddCoffeeCapsule(Models.CoffeeCapsule capsule);

    }
}