using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CofeeNET.Models;

using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CofeeNET.Services
{
    public class ManageCoffeeCapsules : IManageCoffeCapsules
    {
        private readonly Data.MyDataBaseContext _dbContext;

        public ManageCoffeeCapsules(Data.MyDataBaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Models.CoffeeRoom>> GetCoffeeRooms()
        {

            var rooms = _dbContext.CoffeeRooms.Include(cr => cr.CoffeeRoomCapsules);

            List<Models.CoffeeRoom> coffeeRoomList = new List<Models.CoffeeRoom>(rooms);
            return coffeeRoomList;
        }

        public async Task<List<Models.CoffeeCapsule>> GetCoffeeCapsules(int CoffeeRoomID)
        {
            var room = _dbContext.CoffeeRooms
            .Include(cr => cr.CoffeeRoomCapsules)
            .FirstOrDefault(cr => cr.ID == CoffeeRoomID);

            List<Models.CoffeeCapsule> capsuleList = new List<Models.CoffeeCapsule>(room.CoffeeRoomCapsules);
            return capsuleList;
        }

        public async Task<bool> ConsumeCoffeeCapsule(Models.CoffeFlavors capsuleFlavour, int CoffeeRoomID)
        {
            try
            {
                var room = _dbContext.CoffeeRooms
                    .Include(cr => cr.CoffeeRoomCapsules)
                    .FirstOrDefault(cr => cr.ID == CoffeeRoomID);

                room.CoffeeRoomCapsules
                    .FirstOrDefault(c => c.Flavour == capsuleFlavour).CapsulesLeft--;

                int nums = room.CoffeeRoomCapsules
                    .FirstOrDefault(c => c.Flavour == capsuleFlavour).CapsulesLeft;

                if(nums < 0)
                {
                    room.CoffeeRoomCapsules
                    .FirstOrDefault(c => c.Flavour == capsuleFlavour).CapsulesLeft = 0;
                }
                 
                _dbContext.Update(room);
                await _dbContext.SaveChangesAsync();
                return true; 
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> RefillCoffeeCapsules(string capsuleFlavour, int CoffeeRoomID)
        {
            try
            {
                Models.CoffeFlavors flav;
                Enum.TryParse(capsuleFlavour, out flav);

                var room = _dbContext.CoffeeRooms
                    .Include(cr => cr.CoffeeRoomCapsules)
                    .FirstOrDefault(cr => cr.ID == CoffeeRoomID);
                room.CoffeeRoomCapsules
                    .FirstOrDefault(c => c.Flavour == flav).CapsulesLeft = 50;

                _dbContext.Update(room);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Models.CoffeeRoom> AddCoffeeRoom([Bind("Building, Floor")]Models.CoffeeRoom room)
        {
            try
            {
                EntityEntry<CoffeeRoom> newRoom = _dbContext.Add(room);
                await _dbContext.SaveChangesAsync();
                return newRoom.Entity;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Models.CoffeeCapsule> AddCoffeeCapsule([Bind("Flavour, CapsulesLeft")]Models.CoffeeCapsule capsule)
        {
            try
            {
                /*
                Codigo de testeo
                */
                capsule.CoffeeRoom = _dbContext.CoffeeRooms.FirstOrDefault();

                EntityEntry<CoffeeCapsule> caps = _dbContext.Add(capsule);
                await _dbContext.SaveChangesAsync();
                return caps.Entity;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}