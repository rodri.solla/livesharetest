﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using CofeeNET.Models;
using CofeeNET.Services;
using CofeeNET.ViewModels;

namespace CofeeNET.Controllers
{
    public class HomeController : Controller
    {
        IManageCoffeCapsules manageCoffeeCapsules;

        #region Constructor
        public HomeController(IManageCoffeCapsules _manageCoffeeCapsules)
        {
            manageCoffeeCapsules = _manageCoffeeCapsules;
        }
        #endregion 

        #region Index
        public async Task<IActionResult> Index()
        {
            List<CoffeeRoom> coffeeRooms = await manageCoffeeCapsules.GetCoffeeRooms();
            if (coffeeRooms == null)
            {
                return RedirectToAction("Error", new {errorMessage="No coffee rooms found"});
            }
            IndexViewModel viewModel = new IndexViewModel(coffeeRooms);

            return View(viewModel);
        }
        #endregion

        #region Create coffeeRoom
        public IActionResult Create()
        {
            CoffeeRoom coffeeRoom = new CoffeeRoom();
            return View(coffeeRoom);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CoffeeRoom coffeeRoom)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Invalid model");
                return View("Create", coffeeRoom);
            }
            else
            { 
                await manageCoffeeCapsules.AddCoffeeRoom(coffeeRoom);
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region Create coffeeCapsule
        public IActionResult CreateCapsule()
        {
            CoffeeCapsule coffeeCapsule = new CoffeeCapsule();
            return View(coffeeCapsule);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCapsule(CoffeeCapsule coffeeCapsule)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Invalid model");
                return View("CreateCapsule", coffeeCapsule);
            }
            else
            { 
                await manageCoffeeCapsules.AddCoffeeCapsule(coffeeCapsule);
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region Consume

        public async Task<IActionResult> Consume()
        {
            List<CoffeeRoom> coffeeRooms = await manageCoffeeCapsules.GetCoffeeRooms();
            FormViewModel viewModel = new FormViewModel(coffeeRooms);
            return View(viewModel);
        }
        [HttpPost]
        public async Task<IActionResult> Consume([FromForm]FormViewModel input) 
        {
            bool result = await manageCoffeeCapsules.ConsumeCoffeeCapsule(input.SelectedCapsule, input.SelectedRoom);

            if (result)
            {
                return RedirectToAction("Index");
            }
            else 
            {
                return RedirectToAction("Error", new {errorMessage="Error ocurred"});
            }
            
        }
        #endregion 

        #region Refill

        public async Task<IActionResult> Refill([FromQuery] string Flavour, [FromQuery] int Room)
        {
            bool result = await manageCoffeeCapsules.RefillCoffeeCapsules(Flavour, Room);

            if (result)
            {
                return RedirectToAction("Index");
            }
            else 
            {
                return RedirectToAction("Error", new {errorMessage="Error ocurred"});
            }
        }
        #endregion

        #region Delete
        // [HttpPost]
        // public Task<IActionResult> Delete(string coffeRoomId)
        // {
        //     if (string.IsNullOrWhiteSpace(coffeRoomId) || !Guid.TryParse(id, out Guid coffeRoom_guid))
        //     {
        //         return BadRequest();
        //     }
        //     manageCoffeeCapsules.Delete(CoffeeRoom_guid);
        //     return RedirectToAction("Index");
        // }
        #endregion 

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string errorMessage="")
        {
            return View(new ErrorViewModel { 
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier, 
                ErrorMessage=errorMessage 
                });
        }
    }
}