using Microsoft.EntityFrameworkCore;


namespace CofeeNET.Data
{

    public class MyDataBaseContext : DbContext
    {
        public MyDataBaseContext(DbContextOptions<MyDataBaseContext> options) : base(options)
        {
            
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        //entities
        public DbSet<Models.CoffeeCapsule> CoffeeCapsules { get; set; }
        public DbSet<Models.CoffeeRoom> CoffeeRooms { get; set; }
    } 
}
