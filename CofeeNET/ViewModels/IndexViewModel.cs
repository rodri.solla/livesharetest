using System;
using System.Collections.Generic;
using CofeeNET.Models;

namespace CofeeNET.ViewModels
{
    public class IndexViewModel
    {
        public ICollection<IndexRoomViewModel> Rooms;
        public IndexViewModel(ICollection<CoffeeRoom> coffeeRooms)
        {
            Rooms = new List<IndexRoomViewModel>();
            foreach (var room in coffeeRooms)
            {
                Rooms.Add(new IndexRoomViewModel(room));
            }
        }

        public class IndexRoomViewModel
        {
            public string RoomName { get; set; }
            public int RoomId { get; set;}
            public ICollection<IndexCapsulesViewModel> Capsules { get; set; }

            public IndexRoomViewModel(CoffeeRoom coffeeRoom)
            {
                RoomId = coffeeRoom.ID;
                RoomName = coffeeRoom.Building + " - " + coffeeRoom.Floor.ToString();
                Capsules = new List<IndexCapsulesViewModel>();
                foreach (var capsule in coffeeRoom.CoffeeRoomCapsules)
                {
                    Capsules.Add(new IndexCapsulesViewModel(capsule));
                }
            }

            public class IndexCapsulesViewModel 
            {
                public string Flavor { get; set; }
                public int CapsulesLeft { get; set; }

                public IndexCapsulesViewModel(CoffeeCapsule coffeeCapsule)
                {
                    Flavor = coffeeCapsule.Flavour.ToString();
                    CapsulesLeft = coffeeCapsule.CapsulesLeft;
                }
            }
        }
    }
}
