using System;
using System.Collections.Generic;
using CofeeNET.Models;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CofeeNET.ViewModels
{
    public class FormViewModel
    {
        public int SelectedRoom { get; set; }
        public SelectList SLRooms { get; set; }

        public Models.CoffeFlavors SelectedCapsule { get; set; }
        public SelectList SLCapsules { get; set; }

        public FormViewModel(ICollection<CoffeeRoom> rooms)
        {
            SLRooms = new SelectList(rooms, "ID", "Building");
            SLCapsules = new SelectList(rooms.First().CoffeeRoomCapsules, "Flavour", "Flavour");
        }
        public FormViewModel()
        {

        }
    }
}