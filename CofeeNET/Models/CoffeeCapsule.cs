using System;

namespace CofeeNET.Models
{
    public enum CoffeFlavors
    {
        Brazil,
        Colombian,
        Arabic,
        Chocolate,
        Decaffeinated,
        LungoLeggero,
        Ristretto,
        RistrettoIntenso
    }

    public class CoffeeCapsule
    {
        public int ID { get; set;}
        public CoffeFlavors Flavour { get; set; }
        public int CapsulesLeft { get; set; }

        public CoffeeRoom CoffeeRoom { get; set; }
        
    }
}