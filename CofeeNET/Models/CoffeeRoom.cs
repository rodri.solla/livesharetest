using System;
using System.Linq;
using System.Collections.Generic; // IList
using System.Web;


namespace CofeeNET.Models
{
    public class CoffeeRoom
    {
        public CoffeeRoom()
        {
            this.CoffeeRoomCapsules = new List<CoffeeCapsule>();
        }
        public int ID {get; set;}
        public string Building {get; set; }
        public int Floor {get; set;}
        
        public IList<CoffeeCapsule> CoffeeRoomCapsules {get; set;}

    }
}